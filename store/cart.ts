import {useCommonStore} from '@/store/common'
import {useUserStore} from '@/store/user'

interface State {
  cart: cartItem[]
  promocode: Promocode | null
  isShowCartModal: boolean
  discountBlock: DiscountBlock | null
  isShowDiscountBlock: boolean
}

interface cartItem {
  id: number
  name: string
  price: number
  regular_price: number
  image: string
  count: number
  countable: boolean
  measure_unit: string
  portion_nat_size: number
  supplements: Supplement[]
  variation_id: number | null
  locations: number[]
}

interface DiscountBlock {
  header: string,
  description: string,
  buttonColor: string,
  buttonText: string,
  image: string
}
interface Supplement {
  id: number
  name: string
  price: number
  count: number
}

interface Promocode {
  value: string
  type: string
  amount: number
}

export const useCartStore = defineStore('cartStore', {
  state: (): State => ({
    cart: [],
    promocode: null,
    isShowCartModal: false,
    discountBlock: null,
    isShowDiscountBlock: false
  }),

  actions: {
    addToCart (item) {
      const commonStore = useCommonStore()
      if (!commonStore.deliveryType) {
        commonStore.toggleShowDeliveryTypeModal(true)
        return false
      } else if (!commonStore.selectedLocation) {
        commonStore.toggleShowReceiptModal(true)
        return false
      }

      this.cart.push({
        id: item.id,
        name: item.name,
        price: item.price,
        regular_price: item.regular_price,
        image: item.images[0],
        count: 1,
        countable: item.countable,
        measure_unit: item.measure_unit,
        portion_nat_size: item.portion_nat_size,
        supplements: item?.supplements || [],
        variation_id: +item?.variation_id || null,
        locations: item.locations.map(item => item.id),
      })

      commonStore.addNotification({
        type: 'cart',
        text: 'Товар добавлен в корзину',
        status: 'success'
      })

      setTimeout(() => {
        this.getDiscounts()
      }, 2000)
    },

    async getDiscounts () {
      const commonStore = useCommonStore()
      const commonStoreRefs = storeToRefs(commonStore)
      const userStore = useUserStore()
      const userStoreRefs = storeToRefs(userStore)
      const userId = userStoreRefs.user
      console.log("store refs:",commonStoreRefs)
      const deliveryType = commonStoreRefs.deliveryType
      const cartItems = this.cartItems;
      var cartItemsObj: {[k: string]: any} = {};
      const cartItemsNew = cartItems.map(item => {
        const variation_id = item.variation_id === null ? "" : `_${item.variation_id}`
        let product_code = `${item.id}${variation_id}`;
        // cartItemsObj[product_code] = {
        //   product: {
        //     id: `${item.id}`,
        //     name: item.name
        //   },
        //   quantity: item.count,
        //   variation: item.variation_id === null ? 0 : item.variation_id,
        //   price: item.price
        // }
        // cartItemsObj.set(product_code, {
        //   product: {
        //     id: item.id,
        //     name: item.name
        //   },
        //   quantity: item.count,
        //   variation: item.variation_id === null ? 0 : item.variation_id,
        //   price: item.price
        // })
        let result_obj = {
          [product_code]: {
            product: {
              id: item.id,
              name: item.name
            },
            quantity: item.count,
            variation: item.variation_id === null ? 0 : item.variation_id,
            price: item.price
          }
        }
        return result_obj
      });
      // for (var i = 0; i < cartItemsNew.length; ++i) {
      //   const item_key = Object.keys(cartItemsNew[i])[0] as const
      //   cartItemsObj[item_key] = cartItemsNew[i][item_key]
      // }

      console.log("delivery type: ",deliveryType)
      console.log("data object: ",{
        cart_total: this.cartItemsRegularPrice,
        delivery_type: deliveryType.value,
        customer_id: userId.value,
        cart_items: cartItemsNew
      })
      // type discountResponse = {
      //   couponsList: [],
      //   stocksList: [],
      //   cart_bonuses: number
      // }
      const {data} = await useLazyAsyncData(
          'methods',
          () => $fetch('/api/wp-json/wc/v3/coupons/autoapply', {
              method: "POST",
              body: {
                cart_total: this.cartItemsRegularPrice,
                delivery_type: deliveryType,
                customer_id: userId === null ? "" : userId,
                cart_items: cartItemsNew
              },
          })
      )

      const discounts: any | null = data?.value || []
      if (discounts?.couponsList.length > 0) {
        this.discountBlock = {
          header: "Вам доступны скидка или подарок!",
          description: "Вам доступны 3 скидки или подарка. Выберите 1 из предложенных вариантов",
          buttonColor: "yellow",
          buttonText: "Выбрать",
          image: "/images/gift_cart.jpg"
        }
        this.toggleShowDiscountBlock(true)
      }
      console.log("response: ",discounts)

    },

    incrementItem (idx: number) {
      const commonStore = useCommonStore()

      this.cart[idx].count++

      commonStore.addNotification({
        type: 'cart',
        text: 'Товар добавлен в корзину',
        status: 'success'
      })
    },

    decrementItem (idx: number) {
      this.cart[idx].count--

      if (this.cart[idx].count === 0) {
        this.cart.splice(idx, 1)
      }
    },

    removeFromCart (idx: number) {
      this.cart.splice(idx, 1)
    },

    removeMissedProductsFromCart (locationId: number) {
      this.cart = this.cart.filter(item => item.locations.includes(locationId))
    },

    clearCart () {
      this.cart = []
    },

    toggleShowCartModal (value: boolean) {
      this.isShowCartModal = value
    },

    toggleShowDiscountBlock (value: boolean) {
      this.isShowDiscountBlock = value
    },
    setPromocode (value: Promocode | null) {
      this.promocode = value
    },
  },

  getters: {
    cartItems: (state) => {
      return state.cart
    },

    cartItemsLength: (state) => {
      return state.cart.reduce((acc, item) => {
        const count = !item.countable ? 1 : item.count

        acc = acc + count
        return acc
      }, 0)
    },

    // Сумма всех товаров с учетом скидки
    cartItemsPrice: (state) => {
      return state.cart.reduce((acc, item) => {
        let price = +item.price

        if (item.supplements.length) {
          price += item.supplements.reduce((acc2, item2) => {
            acc2 += item2.price * item2.count
            return acc2
          }, 0)
        }

        acc += item.count * price
        return acc
      }, 0)
    },

    // Сумма всех товаров без скидки
    cartItemsRegularPrice: (state) => {
      return state.cart.reduce((acc, item) => {
        let price = +item.regular_price

        if (item.supplements.length) {
          price += item.supplements.reduce((acc2, item2) => {
            acc2 += item2.price * item2.count
            return acc2
          }, 0)
        }

        acc += item.count * price
        return acc
      }, 0)
    },

    productInCart: (state) => {
      return (id: number, supplements: Supplement[] = [], variationId: number | null = null) => {
        let idx = null

        const item = state.cart.find((item, i) => {
          if (
            item.id === id &&
            item?.variation_id === variationId &&
            supplements.length === item.supplements.length &&
            item.supplements.every(itemSupplement => supplements.find(supplement => supplement.id === itemSupplement.id && supplement.count === itemSupplement.count))
          ) {
            idx = i
            return true
          }

          return false
        })

        return {
          item: item || null,
          idx
        }
      }
    },

    missedProductsList: (state) => {
      return (locationId: number) => {
        return state.cart.filter(item => !item.locations.includes(locationId))
      }
    }
  },

  persist: {
    storage: persistedState.localStorage,
    paths: ['cart']
  },
})