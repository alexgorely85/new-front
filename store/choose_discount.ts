import {useCommonStore} from '@/store/common'
interface State {
    discounts: discountItem[]
    isShowChooseDiscountModal: boolean
}
interface discountItem {
    id: number
    name: string
    image: string
    amount: number,
    discountType: discountType
}
enum discountType {
    Percent = "percent",
    FreeGift = "free_gift",
    Amount = "amount",
}
export const useChooseDiscountStore = defineStore('chooseDiscountStore', {
    state: (): State => ({
        discounts: [],
        isShowChooseDiscountModal: false,
    }),
    actions: {
        toggleChooseDiscountModal (value: boolean) {
            this.isShowChooseDiscountModal = value
        },
    },
    getters: {

    },
    persist: {
        storage: persistedState.localStorage,
            paths: ['choose_discount']
    },
})